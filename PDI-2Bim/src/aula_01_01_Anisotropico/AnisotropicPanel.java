/**
 * 
 */
package aula_01_01_Anisotropico;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JPanel;

/**
 * @author MDMatrakas
 * 
 */
@SuppressWarnings("serial")
public class AnisotropicPanel extends JPanel
{
	int maxX;
	int maxY;
	float pixelWidth;
	float pixelHeight;
	float rWidth = 10F;
	float rHeight = 7.5F;
	float xP = -1;
	float yP;
	
	AnisotropicPanel()
	{
		addMouseListener(new MouseAdapter()
			{
				public void mousePressed(MouseEvent e)
				{
					xP = fx(e.getX());
					yP = fy(e.getY());
					repaint();
				}
			}
		);
	}
	
	void initGr()
	{
		Dimension d = getSize();
		maxX = d.width - 1;
		maxY = d.height - 1;
		pixelWidth = rWidth / maxX;
		pixelHeight = rHeight / maxY;
	}
	
	int iX(float x)
	{
		return Math.round(x / pixelWidth);
	}
	
	int iY(float y)
	{
		return maxY - Math.round(y / pixelHeight);
	}
	
	float fx(int x)
	{
		return x * pixelWidth;
	}
	
	float fy(int y)
	{
		return (maxY - y) * pixelHeight;
	}
	
	public void paint(Graphics g)
	{
		super.paint(g);
		
		initGr();
		int left = iX(0);
		int right = iX(rWidth);
		int bottom = iY(0);
		int top = iY(rHeight);
		
		if(xP >= 0)
		{
			g.drawString("Coordenadas l�gicas do ponto selecionado: " + xP + " " + yP, 20, 100);
		}
		g.setColor(Color.red);
		g.drawLine(left, bottom, right, bottom);
		g.drawLine(right, bottom, right, top);
		g.drawLine(right, top, left, top);
		g.drawLine(left, top, left, bottom);
	}
}
