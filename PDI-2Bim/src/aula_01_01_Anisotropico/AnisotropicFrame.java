package aula_01_01_Anisotropico;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;

@SuppressWarnings("serial")
public class AnisotropicFrame extends JFrame
{
	AnisotropicFrame()
	{
		super("Modo de mapeamento anisotrópico.");
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setSize(400, 300);
		add("Center", new AnisotropicPanel());
		setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));
		setVisible(true);
	}
}
