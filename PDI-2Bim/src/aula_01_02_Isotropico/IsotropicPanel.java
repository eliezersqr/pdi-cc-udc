/**
 * 
 */
package aula_01_02_Isotropico;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JPanel;

/**
 * @author MDMatrakas
 *
 */
@SuppressWarnings("serial")
public class IsotropicPanel extends JPanel
{
	int centerX;
	int centerY;
	float pixelSize;
	float rWidth = 10F;
	float rHeight = 10F;
	float xP = 1000000;
	float yP;
	
	IsotropicPanel()
	{
		addMouseListener(new MouseAdapter()
			{
				public void mousePressed(MouseEvent e)
				{
					xP = fx(e.getX());
					yP = fy(e.getY());
					repaint();
				}
			}
		);
	}
	
	void initGr()
	{
		Dimension d = getSize();
		int maxX = d.width - 1;
		int maxY = d.height - 1;
		pixelSize = Math.max(rWidth / maxX, rHeight / maxY);
		centerX = maxX / 2;
		centerY = maxY / 2;
	}
	
	int iX(float x)
	{
		return Math.round(centerX + x / pixelSize);
	}
	
	int iY(float y)
	{
		return Math.round(centerY - y / pixelSize);
	}
	
	float fx(int x)
	{
		return (x - centerX) * pixelSize;
	}
	
	float fy(int y)
	{
		return (centerY - y) * pixelSize;
	}
	
	public void paint(Graphics g)
	{
		super.paint(g);
		
		initGr();
		int left = iX(-rWidth / 2);
		int right = iX(rWidth / 2);
		int bottom = iY(-rHeight / 2);
		int top = iY(rHeight / 2);
		int xMiddle = iX(0);
		int yMiddle = iY(0);
		
		if(xP != 1000000)
		{
			g.drawString("Coordenadas l�gicas do ponto selecionado: x:" + xP + " y:" + yP, 20, 100);
		}
		g.setColor(Color.red);
		g.drawLine(xMiddle, bottom, right, yMiddle);
		g.drawLine(right, yMiddle, xMiddle, top);
		g.drawLine(xMiddle, top, left, yMiddle);
		g.drawLine(left, yMiddle, xMiddle, bottom);
	}
}
