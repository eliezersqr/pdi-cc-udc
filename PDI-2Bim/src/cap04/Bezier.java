/**
 * 
 */
package cap04;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JFrame;

import baseCG.Point2D;

/**
 * @author Matrakas
 *
 */
@SuppressWarnings("serial")
public class Bezier extends JFrame
{
	
	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		new Bezier();		
	}
	
	public Bezier()
	{
		super("Defina pontos extremos e de controle do segmento.");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(500, 300);
		this.add("Center", new CvBezier());
		this.setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));
		this.setVisible(true);
	}	
}

@SuppressWarnings("serial")
class CvBezier extends Canvas
{
	Point2D[] p = new Point2D[4];
	int np = 0, centerX, centerY;
	float rWidth = 10F, rHeight = 7.5F;
	float eps = rWidth / 100F;
	float pixelSize;
	
	CvBezier()
	{
		addMouseListener(new MouseAdapter()
		{
			public void mousePressed(MouseEvent evt)
			{
				float x = fx(evt.getX());
				float y = fy(evt.getY());
				
				if(np == 4)
					np = 0;
				
				p[np++] = new Point2D(x, y);
				repaint();
			}
		}
		);
	}
	
	void initGr()
	{
		Dimension d = getSize();
		int maxX = d.width - 1;
		int maxY = d.height - 1;
		pixelSize = Math.max(rWidth / maxX, rHeight / maxY);
		centerX = maxX / 2;
		centerY = maxY / 2;
	}
	
	int iX(float x)
	{
		return Math.round(centerX + x / pixelSize);
	}
	
	int iY(float y)
	{
		return Math.round(centerY - y / pixelSize);
	}
	
	float fx(int x)
	{
		return (x - centerX) * pixelSize;
	}
	
	float fy(int y)
	{
		return (centerY - y) * pixelSize;
	}
	
	Point2D middle(Point2D a, Point2D b)
	{
		return new Point2D( (a.getX() + b.getX()) / 2, (a.getY() + b.getY()) / 2);
	}
	
	void bezierRec(Graphics g, Point2D p0, Point2D p1, Point2D p2, Point2D p3)
	{
		int x0 = iX(p0.getX());
		int y0 = iY(p0.getY());
		int x3 = iX(p3.getX());
		int y3 = iY(p3.getY());
		
		if(Math.abs(x0 - x3) <= 1 && Math.abs(y0 - y3) <= 1)
			g.drawLine(x0, y0, x3, y3);
		else
		{
			Point2D a = middle(p0, p1);
			Point2D b = middle(p3, p2);
			Point2D c = middle(p1, p2);
			Point2D a1 = middle(a, c);
			Point2D b1 = middle(b, c);
			Point2D c1 = middle(a1, b1);
			
			bezierRec(g, p0, a, a1, c1);
			bezierRec(g, c1, b1, b, p3);
		}
	}
	
	void bezierNRec(Graphics g, Point2D[] p)
	{
		int n = 200;
		float dt = 1F / n;
		float x = p[0].getX();
		float y = p[0].getY();
		float x0, y0;
		
		for(int i = 1; i <= n; i++)
		{
			float t = i * dt;
			float u = 1 - t;
			float tuTriple = 3 * t * u;
			float c0 = u * u * u;
			float c1 = tuTriple * u;
			float c2 = tuTriple * t;
			float c3 = t * t * t;
			
			x0 = x;
			y0 = y;
			
			x = c0 * p[0].getX() + c1 * p[1].getX() + c2 * p[2].getX() + c3 * p[3].getX();
			y = c0 * p[0].getY() + c1 * p[1].getY() + c2 * p[2].getY() + c3 * p[3].getY();
			
			g.drawLine(iX(x0), iY(y0), iX(x), iY(y));
		}
	}
	
	public void paint(Graphics g)
	{
		initGr();
		int left = iX(-rWidth / 2);
		int right = iX(rWidth / 2);
		int top = iY(rHeight / 2);
		int bottom = iY(-rHeight / 2);
		
		g.drawRect(left, top, right - left, bottom - top);
		
		for(int i = 0; i < np; i++)
		{
			// Mostra um pequeno retangulo azul em torno do ponto
			g.setColor(Color.BLUE);
			g.drawRect(iX(p[i].getX()) - 2, iY(p[i].getY()) - 2, 4, 4);
			
			if(i > 0)
			{
				// Desenha reta verde p[i-1]p[i]:
				g.setColor(Color.GREEN);
				g.drawLine(iX(p[i - 1].getX()), iY(p[i - 1].getY()), iX(p[i].getX()), iY(p[i].getY()));
			}
		}
		if(np == 4)
		{
			g.setColor(Color.RED);
			bezierRec(g, p[0], p[1], p[2], p[3]);
			
			
			g.setColor(Color.ORANGE);
			bezierNRec(g, p);
		}
	}
}
