/**
 * 
 */
package cap04;

import java.awt.BorderLayout;
import java.awt.Canvas;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JFrame;

/**
 * @author Matrakas
 *
 */
public class ClipLine extends JFrame
{
	
	/**
	 * 
	 */
	private static final long	serialVersionUID	= 1L;

	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		new ClipLine();		
	}
	
	ClipLine()
	{
		super("Clique em dois vertices opostos de um retangulo");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(500, 300);
		add(new CvClipLine(), BorderLayout.CENTER);
		setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));
		setVisible(true);
	}
}

class CvClipLine extends Canvas
{
	/**
	 * 
	 */
	private static final long	serialVersionUID	= 1L;
	float xMin, xMax, yMin, yMax;
	float rWidth = 10F;
	float rHeight = 7.5F;
	float pixelSize;
	int maxX, maxY, centerX, centerY;
	int np = 0;
	
	CvClipLine()
	{
		addMouseListener(new MouseAdapter()
			{
				public void mousePressed(MouseEvent e)
				{
					float x = fx(e.getX());
					float y = fy(e.getY());
					if(np == 2)
						np = 0;
					if(np == 0)
					{
						xMin = x;
						yMin = y;
					}
					else
					{
						xMax = x;
						yMax = y;
						if(xMax < xMin)
						{
							float t = xMax;
							xMax = xMin;
							xMin = t;
						}
						if(yMax < yMin)
						{
							float t = yMax;
							yMax = yMin;
							yMin = t;
						}
					}
					np++;
					repaint();
				}
			}
		);
	}
	
	void initGr()
	{
		Dimension d = getSize();
		maxX = d.width - 1;
		maxY = d.height - 1;
		pixelSize = Math.max(rWidth/maxX, rHeight/maxY);
		centerX = maxX / 2;
		centerY = maxY / 2;
	}
	
	int iX(float x)
	{
		return Math.round(centerX + x / pixelSize);
	}
	
	int iY(float y)
	{
		return Math.round(centerY - y / pixelSize);
	}
	
	float fx(int x)
	{
		return (x - centerX) * pixelSize;
	}
	
	float fy(int y)
	{
		return (centerY - y) * pixelSize;
	}
	
	void drawLine(Graphics g, float xp, float yp, float xq, float yq)
	{
		g.drawLine(iX(xp), iY(yp), iX(xq), iY(yq));
	}
	
	int clipCode(float x, float y)
	{
		return (x < xMin ? 8 : 0) | (x > xMax ? 4 : 0) | (y < yMin ? 2 : 0) | (y > yMax ? 1 : 0);
	}
	
	void clipLine(Graphics g, float xp, float yp, float xq, float yq, float xMin, float yMin, float xMax, float yMax)
	{
		int cp = clipCode(xp, yp);
		int cq = clipCode(xq, yq);
		float dx, dy;
		
		while((cp | cq) != 0)
		{
			if((cp & cq) != 0)
				return;
			dx = xq - xp;
			dy = yq - yp;
			if(cp != 0)
			{
				if((cp & 8) == 8)
				{
					yp += (xMin - xp) * dy / dx;
					xp = xMin;
				}
				else if((cp & 4) == 4)
				{
					yp += (xMax - xp) * dy /dx;
					xp = xMax;
				}
				else if((cp & 2) == 2)
				{
					xp += (yMin - yp) * dx / dy;
					yp = yMin;
				}
				else if((cp & 1) == 1)
				{
					xp += (yMax - yp) * dx / dy;
					yp = yMax;
				}
				cp = clipCode(xp, yp);
			}
			else if(cq != 0)
			{
				if((cq & 8) == 8)
				{
					yq += (xMin - xq) * dy / dx;
					xq = xMin;
				}
				else if((cq & 4) == 4)
				{
					yq += (xMax - xq) * dy /dx;
					xq = xMax;
				}
				else if((cq & 2) == 2)
				{
					xq += (yMin - yq) * dx / dy;
					yq = yMin;
				}
				else if((cq & 1) == 1)
				{
					xq += (yMax - yq) * dx / dy;
					yq = yMax;
				}
				cq = clipCode(xq, yq);				
			}
		}
		drawLine(g, xp, yp, xq, yq);
	}
	
	public void paint(Graphics g)
	{
		initGr();
		if(np == 1)
		{
			// Desenha linhas horizontais e verticais atrav�s do primeiro ponto definido
			drawLine(g, fx(0), yMin, fx(maxX), yMin);
			drawLine(g, xMin, fy(0), xMin, fy(maxY));
		}
		else if(np == 2)
		{
			// desenha retangulo
			drawLine(g, xMin, yMin, xMax, yMin);
			drawLine(g, xMax, yMin, xMax, yMax);
			drawLine(g, xMax, yMax, xMin, yMax);
			drawLine(g, xMin, yMax, xMin, yMin);
			
			// desenha 20 pentagonos regulares concentricos desde que esles estjan dentro do retangulo
			float rMax = Math.min(rWidth, rHeight) / 2;
			float deltaR = rMax / 20;
			float dPhi = (float)(0.4 * Math.PI);
			
			for(int j = 1; j <= 20; j++)
			{
				float r = j * deltaR;
				// desenha um pentagono
				float xA, yA, xB = r, yB = 0;
				
				for(int i = 1; i <= 5; i++)
				{
					float phi = i * dPhi;
					xA = xB;
					yA = yB;
					xB = (float)(r * Math.cos(phi));
					yB = (float)(r * Math.sin(phi));
					clipLine(g, xA, yA, xB, yB, xMin, yMin, xMax, yMax);
				}
			}
		}
	}
}
