package cap02;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Graphics;

import javax.swing.JFrame;

import baseCG.Point2D;
import baseCG.Tools2D;
import baseCG.Triangle;

import cap01.CvDefPoly;
//import cap01.Point2D;

@SuppressWarnings("serial")
public class PolyTriangFrame extends JFrame
{

	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		new PolyTriangFrame();
	}
	
	PolyTriangFrame()
	{
		super("Clique para definir vertices de um poligono");
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setSize(500, 300);
		add("Center", new CvPolyTriang());
		setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));
		setVisible(true);
	}
}

@SuppressWarnings("serial")
class CvPolyTriang extends CvDefPoly
{
	public void paint(Graphics g)
	{
		int n = v.size();
		int ntr = n - 2;
		
		if(n > 3 && ready)
		{
			Point2D[] p = new Point2D[n];
			
			for(int i = 0; i < n; i++)
			{
				p[i] = (Point2D)v.elementAt(i);			
			}
			// se n�o possuir sentido anti-hor�rio, reverte a ordem
			if(!Tools2D.ccw(p))
				for(int i = 0; i < n; i++)
				{
					p[i] = (Point2D)v.elementAt(n - i - 1);
				}
			
			Triangle[] tr = new Triangle[ntr];
			Tools2D.triangulate(p, tr);
			
			initGr();
			
			for(int i = 0; i < ntr; i++)
			{
				g.setColor(new Color(rand(), rand(), rand()));
				int[] x = new int[3], y = new int[3];
				x[0] = iX(tr[i].getA().getX());
				x[1] = iX(tr[i].getB().getX());
				x[2] = iX(tr[i].getC().getX());
				y[0] = iY(tr[i].getA().getY());
				y[1] = iY(tr[i].getB().getY());
				y[2] = iY(tr[i].getC().getY());
				g.fillPolygon(x, y, 3);
			}
			g.setColor(Color.black);
			g.drawString("Vertices: " + n, 20, 100);
			g.drawString("Triangulos: " + ntr, 20, 115);
			g.drawString("Area: " + Tools2D.area(p), 20, 130);
		}
		g.setColor(Color.black);
		//g.drawString("Vertices: " + n + "\nTriangulos: " + ntr, 20, 100);
		super.paint(g);
	}
	
	int rand()
	{
		return (int)(Math.random() * 256);
	}
}

