package cap01;

import java.awt.Cursor;
//import java.awt.event.WindowAdapter;
//import java.awt.event.WindowEvent;

import javax.swing.JFrame;

@SuppressWarnings("serial")
public class DefPolyFrame extends JFrame
{
	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		new DefPolyFrame();
	}
	
	DefPolyFrame()
	{
		super("Defina os vertices do poligono clicando.");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(500, 300);
		add("Center", new CvDefPoly());
		setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));
		setVisible(true);
	}
}
