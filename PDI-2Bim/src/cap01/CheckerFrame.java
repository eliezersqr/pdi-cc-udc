package cap01;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
//import javax.swing.JOptionPane;

@SuppressWarnings("serial")
public class CheckerFrame extends JFrame
{
	public CheckerFrame()
	{
		super("Checkers");
		initialize();
		addWindowListener(
				new WindowAdapter()
				{
					public void windowClosing(WindowEvent e)
					{
						System.exit(0);
					}
				}
			);
		setSize(200, 100);
		add("Center", new CvCheckersRect());
		setVisible(true);
	}

	/**
	 * This method initializes this
	 * 
	 */
	private void initialize() {
        this.setSize(new Dimension(264, 173));
			
	}

	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		new CheckerFrame();

	}

}  //  @jve:decl-index=0:visual-constraint="10,10"

@SuppressWarnings("serial")
class CvCheckersRect extends Canvas
{
	int n;
	int w;
	int x;
	int y;
	
	CvCheckersRect()
	{
		x = 1;
		y = 1;
		n = 8;
		w = 1;
	}
	public void paint(Graphics g)
	{
		Dimension d = getSize();
		x = (int)(d.width * 0.1);
		y = (int)(d.height * 0.1);
		n = 8;
		w = d.width > d.height ? (d.height - (int)(d.height * 0.2)) / n : (d.width - (int)(d.width * 0.2)) / n;

		//JOptionPane.showMessageDialog(null, String.format("Largura %d\nAltura %d\nx %d\ny %d\nw %d", d.width, d.height, x, y, w));
		for(int i = 0; i < n; i++)
		{
			for(int j = 0; j < n; j++)
			{
				g.setColor((i + n - j) % 2 == 0 ? Color.LIGHT_GRAY : Color.DARK_GRAY);
				g.fillRect(x + i * w, y + j * w, w, w);
			}
		}
	}
}

