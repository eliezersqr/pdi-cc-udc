package cap01;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;

@SuppressWarnings("serial")
public class RedRectFrame extends JFrame
{

	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		new RedRectFrame();
	}
	
	RedRectFrame()
	{
		super("RedRect");
		addWindowListener(
				new WindowAdapter()
				{
					public void windowClosing(WindowEvent e)
					{
						System.exit(0);
					}
				}
			);
		setSize(200, 100);
		add("Center", new CvRedRect());
		setVisible(true);
	}
}

@SuppressWarnings("serial")
class CvRedRect extends Canvas
{
	public void paint(Graphics g)
	{
		Dimension d = getSize();
		int maxX = d.width - 1;
		int maxY = d.height - 1;
		g.drawString("d.largura = " + d.width, 10, 30);
		g.drawString("d.altura = " + d.height, 10, 60);
		g.setColor(Color.red);
		g.drawRect(0, 0, maxX, maxY);
	}
}
