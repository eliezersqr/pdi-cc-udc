package baseCG;

public class Point3D
{
	private float y;
	private float x;
	private float z;
	
	public Point3D(float x, float y, float z)
	{
		this.setX(x);
		this.setY(y);
		this.setZ(z);
	}
	public void setX(float x)
	{
		this.x = x;
	}
	public float getX()
	{
		return x;
	}
	public void setY(float y)
	{
		this.y = y;
	}
	public float getY()
	{
		return y;
	}
	public void setZ(float z)
	{
		this.z = z;
	}
	public float getZ()
	{
		return z;
	}

}
