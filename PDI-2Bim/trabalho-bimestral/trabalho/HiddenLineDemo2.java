package trabalho;
// HLines.java: Perspective drawing with hidden-line elimination.
// Uses: Point2D, Point3D, Tools2D, Obj3D, Input, Polygon3D, Tria.

/* CGDemo is a companion of the textbook

L. Ammeraal and K. Zhang, Computer Graphics for Java Programmers, 
2nd Edition, Wiley, 2006.

Copyright (C) 2006  Janis Schubert, Kang Zhang, Leen Ammeraal 

This program is free software; you can redistribute it and/or 
modify it under the terms of the GNU General Public License as 
published by the Free Software Foundation; either version 2 of 
the License, or (at your option) any later version. 

This program is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of 
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
See the GNU General Public License for more details.  

You should have received a copy of the GNU General Public 
License along with this program; if not, write to 
the Free Software Foundation, Inc., 51 Franklin Street, 
Fifth Floor, Boston, MA  02110-1301, USA. 
*/

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.io.*;

import javax.swing.JFrame;

public class HiddenLineDemo2 extends JFrame //Panel
{  /**
	 * 
	 */
	private static final long	serialVersionUID	= 1L;

public static void main(String[] args)
   {  new HiddenLineDemo2(args.length > 0 ? args[0] : null);
   }
   private MenuItem open, exportHPGL, exit, eyeUp, eyeDown,
      eyeLeft, eyeRight, incrDist, decrDist;
   private CvHLines cv;
   private String sDir;

   public HiddenLineDemo2(String argFileName)
   {  //super("Hidden-lines algorithm");
      cv = new CvHLines();
//    addWindowListener(new WindowAdapter()
//    {public void windowClosing(WindowEvent e){System.exit(0);}});
    addWindowListener(new WindowAdapter()
    {public void windowClosing(WindowEvent e){System.exit(0);}});
      cv.addMouseListener(new MouseAdapter() {
      	public void mouseClicked(MouseEvent e) {
      		cv.advance_algo();
      		cv.repaint();
      	}
      });

      final Button test = new Button("Test Off");
      test.addMouseListener(new MouseAdapter() {
		public void mouseClicked(MouseEvent e) {
			if (cv.detailed_mode) {
				test.setLabel("Test On");
				cv.detailed_mode = false;
				cv.repaint();
			} else {
				test.setLabel("Test Off");
				cv.detailed_mode = true;
				cv.init();
		      	cv.setup_paint();
		      	cv.repaint();
			}
		}
	});

      final Button animate = new Button("Triangulate");
      animate.addMouseListener(new MouseAdapter() {
		public void mouseClicked(MouseEvent e) {
			if (!cv.animation_mode) {
				animate.setLabel("Stop");
				cv.thread.reset();
				cv.animation_mode = true;
				cv.repaint();
			} else {
				animate.setLabel("Triangulate");
				cv.animation_mode = false;
				cv.thread.reset();
		      	cv.repaint();
			}
		}
	});

      Panel p = new Panel();
      p.add(test);
      p.add(animate);
      MenuBar mBar = new MenuBar();
      setMenuBar(mBar);
      Menu mF = new Menu("File"), mV = new Menu("View");
      mBar.add(mF); mBar.add(mV);

      open = new MenuItem("Open", new MenuShortcut(KeyEvent.VK_O));
      exportHPGL = new MenuItem("Export HP-GL");
      exit = new MenuItem("Exit", new MenuShortcut(KeyEvent.VK_Q));
      eyeDown = new MenuItem("Viewpoint Down",
         new MenuShortcut(KeyEvent.VK_DOWN));
      eyeUp = new MenuItem("Viewpoint Up",
         new MenuShortcut(KeyEvent.VK_UP));
      eyeLeft = new MenuItem("Viewpoint to Left",
         new MenuShortcut(KeyEvent.VK_LEFT));
      eyeRight = new MenuItem("Viewpoint to Right",
         new MenuShortcut(KeyEvent.VK_RIGHT));

      incrDist = new MenuItem("Increase viewing distance",
         new MenuShortcut(KeyEvent.VK_INSERT));
      decrDist = new MenuItem("Decrease viewing distance",
         new MenuShortcut(KeyEvent.VK_DELETE));
      mF.add(open); mF.add(exportHPGL); mF.add(exit);
      mV.add(eyeDown); mV.add(eyeUp);
      mV.add(eyeLeft); mV.add(eyeRight);
      mV.add(incrDist); mV.add(decrDist);
      MenuCommands mListener = new MenuCommands();
      open.addActionListener(mListener);
      exportHPGL.addActionListener(mListener);
      exit.addActionListener(mListener);
      eyeDown.addActionListener(mListener);
      eyeUp.addActionListener(mListener);
      eyeLeft.addActionListener(mListener);
      eyeRight.addActionListener(mListener);
      incrDist.addActionListener(mListener);
      decrDist.addActionListener(mListener);
      add("Center", cv);
      add("South",p);
      Dimension dim = getToolkit().getScreenSize();
      setSize(dim.width/2, dim.height/2);
      setLocation(dim.width/4, dim.height/4);
      if (argFileName != null)
      {  Obj3D obj = new Obj3D();
         if (obj.read(argFileName)){cv.setObj(obj); cv.repaint();}
      }
      //show();
   }

   void vp(float dTheta, float dPhi, float fRho) // Viewpoint
   {  Obj3D obj = cv.getObj();
      if (obj == null || !obj.vp(cv, dTheta, dPhi, fRho)) {
         Toolkit.getDefaultToolkit().beep();
      }
      else {
      	cv.init();
      	cv.setup_paint();
      	cv.repaint();
      }
   }

   class MenuCommands implements ActionListener
   {  public void actionPerformed(ActionEvent ae)
      {  if (ae.getSource() instanceof MenuItem)
         {  MenuItem mi = (MenuItem)ae.getSource();
            if (mi == open)
            {  FileDialog fDia = new FileDialog(HiddenLineDemo2.this,
                  "Open", FileDialog.LOAD);
               fDia.setDirectory(sDir);
               fDia.setFile("*.dat");
              // fDia.show();
               String sDir1 = fDia.getDirectory();
               String sFile = fDia.getFile();
               String fName = sDir1 + sFile;
               Obj3D obj = new Obj3D();
               if (obj.read(fName))
               {  sDir = sDir1;
                  cv.setObj(obj);
                  cv.repaint();
               }
            }
            else
            if (mi == exportHPGL)
            {  Obj3D obj = cv.getObj();
               if (obj != null)
               {  cv.setHPGL(new myHPGL(obj));
                  cv.repaint();
               }
               else
                  Toolkit.getDefaultToolkit().beep();
            }
            else
            if (mi == exit) System.exit(0); else
            if (mi == eyeDown) vp(0, .1F, 1); else
            if (mi == eyeUp) vp(0, -.1F, 1); else
            if (mi == eyeLeft) vp(-.1F, 0, 1); else
            if (mi == eyeRight) vp(.1F, 0, 1); else
            if (mi == incrDist) vp(0, 0, 2); else
            if (mi == decrDist) vp(0, 0, .5F);
         }
      }
   }
}

// Class CvHLines:
// ===============

class CvHLines extends Canvas
{  /**
	 * 
	 */
	private static final long	serialVersionUID	= 1L;
private int maxX, maxY, centerX, centerY, nTria, nVertices;
   private Obj3D obj;
   private Point2D imgCenter;
   private Tria[] tr;
   private myHPGL hpgl;
   private int[] refPol;
   private int[][] connect;
   private int[] nConnect;
   private int chunkSize = 4;
   private double hLimit;
   private Vector<Polygon3D> polyList;
   private float maxScreenRange;

   private Vector<EvaluationSegment> segments;
   int currentTest;
   int currentTria;
   private Vector<CompletedSegment> shown;
   private Vector<CompletedSegment> hidden;
   boolean all_lines_processed;

   boolean detailed_mode = true;
   boolean animation_mode = false;
   AnimationThread thread;

   Obj3D getObj(){return obj;}
   void setObj(Obj3D obj){this.obj = obj;}
   void setHPGL(myHPGL hpgl){this.hpgl = hpgl;}

   public CvHLines() {
   	init();
   	thread = new AnimationThread(this);
   	thread.start();
   }
   public void init() {
   	currentTest = 0;
   	currentTria = -1;
   	shown = new Vector<CompletedSegment>();
   	hidden = new Vector<CompletedSegment>();
   	segments = new Vector<EvaluationSegment>();
   	all_lines_processed = false;

   }

   //Execute the next test in the sequence by calling the lineSegment
   //method on the segment currently under consideration.
   //If all the triangles have been exhausted, after the test,
   //declare the line segment visible and reset the triangle
   //and test number for the next segment.
   public void advance_algo() {
   	if (!detailed_mode)
   		return;
   	if (segments.size() > 0) {
   		EvaluationSegment es = segments.elementAt(0);
   		lineSegment(es.Pe, es.Qe, es.PScr, es.QScr, es.iP, es.iQ, es.iStart, currentTest);
   		es.iStart = currentTria;
   		if (currentTria >= tr.length) {
   			segments.remove(0);
   			shown.add(new CompletedSegment(es.PScr, es.QScr));
   			currentTria = 0;
   			currentTest = 1;
   		}
   		if (segments.size() == 0) {
			all_lines_processed = true;
		}
   	}
   }

   //Draw the triangulation of each polygon using a simple animation
   //thread.  Switch colors whenever a new polygon is encountered.
   public void anim_paint(Graphics g) {
   	int frame = thread.currentFrame;
		Color[] cols = new Color[] {Color.RED, Color.ORANGE, Color.YELLOW,
									Color.GREEN, Color.BLUE,Color.MAGENTA
		};
		int color_marker = 0;
		Point2D[] vScr = obj.getVScr();
		int poly = refPol[0];

			for (int j = 0; j < frame; j++) {
				if (refPol[j] != poly) {
					color_marker++;
					if (color_marker >= cols.length)
						color_marker = 0;
				}
				poly = refPol[j];
				g.setColor(cols[color_marker]);
				Tria t = tr[j];
		   		int iA = t.iA, iB = t.iB, iC = t.iC;
		         Point2D AScr = vScr[iA], BScr = vScr[iB], CScr = vScr[iC];
		         drawLine(g, AScr.x, AScr.y, BScr.x, BScr.y);
		         drawLine(g, CScr.x, CScr.y, BScr.x, BScr.y);
		         drawLine(g, AScr.x, AScr.y, CScr.x, CScr.y);
			}
	}

   //Paint method for use in the test mode.
   public void paint(Graphics g) {
   	if (!detailed_mode) {
   		old_paint(g);
   		return;
   	}

   	if (!all_lines_processed && obj != null && segments.size() == 0) {
   		setup_paint();
   	}

   	if (obj == null)
			return;

   	Point2D[] vScr = obj.getVScr();
   	//Draw all the line segments which have yet to be evaluated
   	//in green.
   	for (int i = 1; i < segments.size(); i++) {
   		EvaluationSegment es = segments.elementAt(i);
   		g.setColor(Color.GREEN);
   		drawLine(g, es.PScr.x, es.PScr.y, es.QScr.x, es.QScr.y);
   	}

   	//Draw all line segments which have been established to be not
   	//hidden in black.
   	for (int i = 0; i < shown.size(); i++) {
   		CompletedSegment ss = shown.elementAt(i);
   		g.setColor(Color.BLUE);
   		drawLine(g, ss.PScr.x, ss.PScr.y, ss.QScr.x, ss.QScr.y);
   	}

   	//Draw all line segments which are hidden as dashed grey lines.
   	for (int i = 0; i < hidden.size(); i++) {
   		CompletedSegment hs = hidden.elementAt(i);
   		g.setColor(Color.LIGHT_GRAY);
   		drawLine(g, hs.PScr.x, hs.PScr.y, hs.QScr.x, hs.QScr.y);
   		g.setColor(Color.GRAY);
   		drawLine(g, hs.PScr.x, hs.PScr.y, hs.QScr.x, hs.QScr.y,true);

   	}
   	//if there's still processing to do...
   	if (!all_lines_processed) {

   	//Draw the current triangle under consideration in yellow.
   	if (currentTria != -1) {
   		Tria t = tr[currentTria];
   		int iA = t.iA, iB = t.iB, iC = t.iC;
         Point2D AScr = vScr[iA], BScr = vScr[iB], CScr = vScr[iC];
         g.setColor(Color.YELLOW);
         drawLine(g, AScr.x, AScr.y, BScr.x, BScr.y);
         drawLine(g, CScr.x, CScr.y, BScr.x, BScr.y);
         drawLine(g, AScr.x, AScr.y, CScr.x, CScr.y);
   	}

   	//Draw the line segment currently under consideration in red.
   	if (segments.size() > 0) {
   		EvaluationSegment es = segments.elementAt(0);
   		g.setColor(Color.RED);
   		drawLine(g, es.PScr.x, es.PScr.y, es.QScr.x, es.QScr.y);
   		drawLine(g, es.PScr.x-1, es.PScr.y-1, es.QScr.x-1, es.QScr.y-1);
   	}

   	//Draw the current test being executed in black text.
   	if (currentTest > 0) {
			g.setColor(Color.BLACK);
			g.drawString("Test " + currentTest, this.maxX-100,20);
		}
   	}

   	if (animation_mode)
   		anim_paint(g);

   }

   //Sets up the test mode for painting, by building a list of all
   //the line segments which must be considered.
   public void setup_paint()
   {
   	if (!detailed_mode)
   		return;
   	if (obj == null) return;
      Vector<Polygon3D> polyList = obj.getPolyList();
      if (polyList == null) return;
      int nFaces = polyList.size();
      if (nFaces == 0) return;
      @SuppressWarnings("unused")
	float xe, ye, ze;
      Dimension dim = getSize();
      maxX = dim.width - 1; maxY = dim.height - 1;
      centerX = maxX/2; centerY = maxY/2;
      currentTest = 1;
      currentTria = 0;
      // ze-axis towards eye, so ze-coordinates of
      // object points are all negative. Since screen
      // coordinates x and y are used to interpolate for
      // the z-direction, we have to deal with 1/z instead
      // of z. With negative z, a small value of 1/z means
      // a small value of |z| for a nearby point.

      // obj is a java object that contains all data,
      // with w, e and vScr parallel (with vertex numbers
      // as index values):
      // - Vector w (with Point3D elements)
      // - Array e (with Point3D elements)
      // - Array vScr (with Point2D elements)
      // - Vector polyList (with Polygon3D elements)

      // Every Polygon3D value contains:
      // - Array 'nrs' for vertex numbers (n elements)
      // - Values a, b, c, h for the plane ax+by+cz=h.
      // - Array t (with n-2 elements of type Tria)

      // Every Tria value consists of the three vertex
      // numbers A, B and C.
      maxScreenRange = obj.eyeAndScreen(dim);
      imgCenter = obj.getImgCenter();
      obj.planeCoeff();      // Compute a, b, c and h.

      hLimit = -1e-6 * obj.getRho();
      buildLineSet();

      // Construct an array of triangles in
      // each polygon and count the total number
      // of triangles.
      nTria = 0;
      for (int j=0; j<nFaces; j++)
      {  Polygon3D pol = (Polygon3D)(polyList.elementAt(j));
         if (pol.getNrs().length > 2 && pol.getH() <= hLimit)
         {  pol.triangulate(obj);
            nTria += pol.getT().length;
         }
      }
      tr = new Tria[nTria];    // Triangles of all polygons
      refPol = new int[nTria]; // tr[i] belongs to polygon refPol[i]
      int iTria = 0;
      for (int j=0; j<nFaces; j++)
      {  Polygon3D pol = (Polygon3D)(polyList.elementAt(j));
         Tria[] t = pol.getT(); // Triangles of one polygon
         if (pol.getNrs().length > 2 && pol.getH() <= hLimit)
         {  for (int i=0; i<t.length; i++)
            {  Tria tri = t[i];
               tr[iTria] = tri;
               refPol[iTria++] = j;
            }
         }
      }
      if (thread.limit != tr.length)
      	thread.init(tr.length);
      Point3D[] e = obj.getE();
      Point2D[] vScr = obj.getVScr();
      segments = new Vector<EvaluationSegment>();
      hidden = new Vector<CompletedSegment>();
      shown = new Vector<CompletedSegment>();
      for (int i=0; i<nVertices; i++)
      {  for (int j=0; j<nConnect[i]; j++)
         {  int jj = connect[i][j];
      		segments.add(new EvaluationSegment(e[i], e[jj], vScr[i], vScr[jj], i, jj, 0));
         }
      }
      hpgl = null;
   }

   // "Old" paint method for use in non-test mode.
   public void old_paint(Graphics g)
   {  if (obj == null) return;
      Vector<Polygon3D> polyList = obj.getPolyList();
      if (polyList == null) return;
      int nFaces = polyList.size();
      if (nFaces == 0) return;
      @SuppressWarnings("unused")
	float xe, ye, ze;
      Dimension dim = getSize();
      maxX = dim.width - 1; maxY = dim.height - 1;
      centerX = maxX/2; centerY = maxY/2;
      // ze-axis towards eye, so ze-coordinates of
      // object points are all negative. Since screen
      // coordinates x and y are used to interpolate for
      // the z-direction, we have to deal with 1/z instead
      // of z. With negative z, a small value of 1/z means
      // a small value of |z| for a nearby point.

      // obj is a java object that contains all data,
      // with w, e and vScr parallel (with vertex numbers
      // as index values):
      // - Vector w (with Point3D elements)
      // - Array e (with Point3D elements)
      // - Array vScr (with Point2D elements)
      // - Vector polyList (with Polygon3D elements)

      // Every Polygon3D value contains:
      // - Array 'nrs' for vertex numbers (n elements)
      // - Values a, b, c, h for the plane ax+by+cz=h.
      // - Array t (with n-2 elements of type Tria)

      // Every Tria value consists of the three vertex
      // numbers A, B and C.
      maxScreenRange = obj.eyeAndScreen(dim);
      imgCenter = obj.getImgCenter();
      obj.planeCoeff();      // Compute a, b, c and h.

      hLimit = -1e-6 * obj.getRho();
      buildLineSet();

      // Construct an array of triangles in
      // each polygon and count the total number
      // of triangles.
      nTria = 0;
      for (int j=0; j<nFaces; j++)
      {  Polygon3D pol = (Polygon3D)(polyList.elementAt(j));
         if (pol.getNrs().length > 2 && pol.getH() <= hLimit)
         {  pol.triangulate(obj);
            nTria += pol.getT().length;
         }
      }
      tr = new Tria[nTria];    // Triangles of all polygons
      refPol = new int[nTria]; // tr[i] belongs to polygon refPol[i]
      int iTria = 0;

      for (int j=0; j<nFaces; j++)
      {  Polygon3D pol = (Polygon3D)(polyList.elementAt(j));
         Tria[] t = pol.getT(); // Triangles of one polygon
         if (pol.getNrs().length > 2 && pol.getH() <= hLimit)
         {  for (int i=0; i<t.length; i++)
            {  Tria tri = t[i];
               tr[iTria] = tri;
               refPol[iTria++] = j;
            }
         }
      }
      if (thread.limit != tr.length)
      	thread.init(tr.length);
      Point3D[] e = obj.getE();
      Point2D[] vScr = obj.getVScr();
      for (int i=0; i<nVertices; i++)
      {  for (int j=0; j<nConnect[i]; j++)
         {  int jj = connect[i][j];
            oldLineSegment(g, e[i], e[jj], vScr[i], vScr[jj], i, jj, 0);
         }
      }
      if (animation_mode) {
      	anim_paint(g);
      }
      hpgl = null;
   }
   private void buildLineSet()
   {  // Build the array
      // 'connect' of int arrays, where
      // connect[i] is the array of all
      // vertex numbers j, such that connect[i][j] is
      // an edge of the 3D object.
      polyList = obj.getPolyList();
      nVertices = obj.getVScr().length;
      connect = new int[nVertices][];
      nConnect = new int[nVertices];
      for (int i=0; i<nVertices; i++)
      nConnect[i] = 0;
      int nFaces = polyList.size();

      for (int j=0; j<nFaces; j++)
      {  Polygon3D pol = (Polygon3D)(polyList.elementAt(j));
         int[] nrs = pol.getNrs();
         int n = nrs.length;
         if (n > 2 && pol.getH() > 0) continue;
         int ii = Math.abs(nrs[n-1]);
         for (int k=0; k<n; k++)
         {  int jj = nrs[k];
            if (jj < 0)
               jj = -jj; // abs
            else
            {  int i1 = Math.min(ii, jj), j1 = Math.max(ii, jj),
                   nCon = nConnect[i1];
               // Look if j1 is already present:
               int l;
               for (l=0; l<nCon; l++) if (connect[i1][l] == j1) break;
               if (l == nCon)  // Not found:
               {  if (nCon % chunkSize == 0)
                  {  int[] temp = new int[nCon + chunkSize];
                     for (l=0; l<nCon; l++) temp[l] = connect[i1][l];
                     connect[i1] = temp;
                  }
                  connect[i1][nConnect[i1]++] = j1;
               }
            }
            ii = jj;
         }
      }
   }

   int iX(float x){return Math.round(centerX + x - imgCenter.x);}
   int iY(float y){return Math.round(centerY - y + imgCenter.y);}

   private String toString(float t)
   // From screen device units (pixels) to HP-GL units (0-10000):
   {  int i = Math.round(5000 + t * 9000/maxScreenRange);
      String s = "";
      int n = 1000;
      for (int j=3; j>=0; j--)
      {  s += i/n;
         i %= n;
         n /= 10;
      }
      return s;
   }

   private String hpx(float x){return toString(x - imgCenter.x);}
   private String hpy(float y){return toString(y - imgCenter.y);}

   private void drawLine(Graphics g, float x1, float y1,
      float x2, float y2)
   {  if (x1 != x2 || y1 != y2)
      {  g.drawLine(iX(x1), iY(y1), iX(x2), iY(y2));
         if (hpgl != null)
         {  hpgl.write("PU;PA" + hpx(x1) + "," + hpy(y1));
            hpgl.write("PD;PA" + hpx(x2) + "," + hpy(y2) + "\n");
         }
      }
   }

   //Modified drawLine method which is capable of drawing dashed lines.
   private void drawLine(Graphics g, float x1, float y1,
        float x2, float y2, boolean dashed)
     {  if (x1 != x2 || y1 != y2)
        {
     	if (!dashed) {
     		g.drawLine(iX(x1), iY(y1), iX(x2), iY(y2));
     	} else {
     		HiddenLineDemo2Lines.dashedLine(g, iX(x1), iY(y1), iX(x2), iY(y2), 3);
     	}
           if (hpgl != null)
           {  hpgl.write("PU;PA" + hpx(x1) + "," + hpy(y1));
              hpgl.write("PD;PA" + hpx(x2) + "," + hpy(y2) + "\n");
           }
        }
     }

   private void increment_triangle() {
   	currentTria++;
   	currentTest = 1;
   }

   //Modified lineSegment method. Only executes a single test, and
   //as a result of each test either advances to the next test
   //(which will be executed on a subsequent call), advances to the
   //next triangle under consideration, adds a new line subsegment
   //to be considered later, or declares a line segment to be hidden.
   //
   private void lineSegment(Point3D Pe, Point3D Qe,
      Point2D PScr, Point2D QScr, int iP, int iQ, int iStart, int test)
   {  Point2D IScr = null;
   	  Point2D JScr = null;
   	  double u1 = QScr.x - PScr.x, u2 = QScr.y - PScr.y;
      double minPQx = Math.min(PScr.x, QScr.x);
      double maxPQx = Math.max(PScr.x, QScr.x);
      double minPQy = Math.min(PScr.y, QScr.y);
      double maxPQy = Math.max(PScr.y, QScr.y);
      double zP = Pe.z, zQ = Qe.z;
      double minPQz = Math.min(zP, zQ);
      Point3D[] e = obj.getE();
      Point2D[] vScr = obj.getVScr();
      	currentTria = iStart;
      	if (currentTria >= tr.length)
      		return;
         Tria t = tr[currentTria];
         int iA = t.iA, iB = t.iB, iC = t.iC;
         Point2D AScr = vScr[iA], BScr = vScr[iB], CScr = vScr[iC];
         @SuppressWarnings("unused")
		boolean segment = false;
         boolean PInside =
            Tools2D.insideTriangle(AScr, BScr, CScr, PScr);
         boolean QInside =
            Tools2D.insideTriangle(AScr, BScr, CScr, QScr);
         double eps = 0.1;

         int iPol = refPol[currentTria];
         Polygon3D pol = (Polygon3D)polyList.elementAt(iPol);
         double a = pol.getA(), b = pol.getB(), c = pol.getC(),
            h = pol.getH(), eps1 = 1e-5 * Math.abs(h),
            hP = a * Pe.x + b * Pe.y + c * Pe.z,
            hQ = a * Qe.x + b * Qe.y + c * Qe.z;
         double h1 = h + eps1;
         switch (test) {
         case 1:
         // 1. Minimax test for x and y screen coordinates:
         if (maxPQx <= AScr.x && maxPQx <= BScr.x && maxPQx <= CScr.x
          || minPQx >= AScr.x && minPQx >= BScr.x && minPQx >= CScr.x
          || maxPQy <= AScr.y && maxPQy <= BScr.y && maxPQy <= CScr.y
          || minPQy >= AScr.y && minPQy >= BScr.y && minPQy >= CScr.y) {
             increment_triangle();

         } else {
         	currentTest++;
         }
         return;
         case 2:

         // 2. Test if PQ is an edge of ABC:
         if ((iP == iA || iP == iB || iP == iC) &&
             (iQ == iA || iQ == iB || iQ == iC)) {
         	increment_triangle();
         } else {
         	currentTest++;
         }
         return;
         case 3:
         // 3. Test if PQ is clearly nearer than ABC:
         Point3D Ae = e[iA], Be = e[iB], Ce = e[iC];
         double zA = Ae.z, zB = Be.z, zC = Ce.z;
         if (minPQz >= zA && minPQz >= zB && minPQz >= zC) {
         	increment_triangle();
         } else {
         	currentTest++;
         }
         return;
         case 4:

         // 4. Do P and Q (in 2D) lie in a half plane defined
         //    by line AB, on the side other than that of C?
         //    Similar for the edges BC and CA.
          // Relative to numbers of pixels
         if (Tools2D.area2(AScr, BScr, PScr) < eps &&
             Tools2D.area2(AScr, BScr, QScr) < eps ||
             Tools2D.area2(BScr, CScr, PScr) < eps &&
             Tools2D.area2(BScr, CScr, QScr) < eps ||
             Tools2D.area2(CScr, AScr, PScr) < eps &&
             Tools2D.area2(CScr, AScr, QScr) < eps) {
         	increment_triangle();
         } else {
         	currentTest++;
         }
         return;
         case 5:
         // 5. Test (2D) if A, B and C lie on the same side
         //    of the infinite line through P and Q:
         double PQA = Tools2D.area2(PScr, QScr, AScr);
         double PQB = Tools2D.area2(PScr, QScr, BScr);
         double PQC = Tools2D.area2(PScr, QScr, CScr);

         if (PQA < +eps && PQB < +eps && PQC < +eps ||
             PQA > -eps && PQB > -eps && PQC > -eps) {
            increment_triangle();
         } else {
         	currentTest++;
         }
         return;
         case 6:
         // 6. Test if neither P nor Q lies behind the
         //    infinite plane through A, B and C:

         if (hP > h - eps1 && hQ > h - eps1) {
            increment_triangle();
         } else {
         	currentTest++;
         }
         return;
         case 7:
         // 7. Test if both P and Q behind triangle ABC:

         if (PInside && QInside) {
         	hidden.add(new CompletedSegment(PScr, QScr));

         	segments.remove(0);
         	currentTria = 0;
         	currentTest = 1;
         } else {
         	currentTest++;
         }
         return;

         case 8:
         // 8. If P nearer than ABC and inside, PQ visible;
         //    the same for Q:

         boolean PNear = hP > h1, QNear = hQ > h1;
         if (PNear && PInside || QNear && QInside) {
         	increment_triangle();
         } else {
         	currentTest++;
         }
         return;

         // 9. Compute the intersections I and J of PQ
         // with ABC in 2D.
         // If, in 3D, such an intersection lies in front of
         // ABC, this triangle does not obscure PQ.
         // Otherwise, the intersections lie behind ABC and
         // this triangle obscures part of PQ:
         case 9:
         double lambdaMin = 1.0, lambdaMax = 0.0;
         for (int ii=0; ii<3; ii++)
         {  double v1 = BScr.x - AScr.x, v2 = BScr.y - AScr.y,
                   w1 = AScr.x - PScr.x, w2 = AScr.y - PScr.y,
                   denom = u2 * v1 - u1 * v2;
            if (denom != 0)
            {  double mu = (u1 * w2 - u2 * w1)/denom;
               // mu = 0 gives A and mu = 1 gives B.
               if (mu > -0.0001 && mu < 1.0001)
               {  double lambda = (v1 * w2 - v2 * w1)/denom;
                  // lambda = PI/PQ
                  // (I is point of intersection)
                  if (lambda > -0.0001 && lambda < 1.0001)
                  {  if (PInside != QInside &&
                     lambda > 0.0001 && lambda < 0.9999)
                     {  lambdaMin = lambdaMax = lambda;
                        break;
                        // Only one point of intersection
                     }
                     if (lambda < lambdaMin) lambdaMin = lambda;
                     if (lambda > lambdaMax) lambdaMax = lambda;
                  }
               }
            }
            Point2D temp = AScr; AScr = BScr;
            BScr = CScr; CScr = temp;
         }
         float d = obj.getD();
         if (!PInside && lambdaMin > 0.001)
         {  double IScrx = PScr.x + lambdaMin * u1,
                   IScry = PScr.y + lambdaMin * u2;
            // Back from screen to eye coordinates:
            double zI = 1/(lambdaMin/zQ + (1 - lambdaMin)/zP),
                   xI = -zI * IScrx / d, yI = -zI * IScry / d;
            if (a * xI + b * yI + c * zI > h1) {
            	increment_triangle();
            	return;
            }
            IScr = new Point2D((float)IScrx, (float)IScry);
            if (Tools2D.distance2(IScr, PScr) >= 1.0) {
            	  segments.add(1,new EvaluationSegment(Pe, new Point3D(xI, yI, zI), PScr,
                  IScr, iP, -1, currentTria + 1));
            }
         }
         if (!QInside && lambdaMax < 0.999)
         {  double JScrx = PScr.x + lambdaMax * u1,
                   JScry = PScr.y + lambdaMax * u2;
            double zJ =
               1/(lambdaMax/zQ + (1 - lambdaMax)/zP),
                  xJ = -zJ * JScrx / d, yJ = -zJ * JScry / d;
            if (a * xJ + b * yJ + c * zJ > h1) {
            	increment_triangle();
            	return;
            }
            JScr = new Point2D((float)JScrx, (float)JScry);
            if (Tools2D.distance2(JScr, QScr) >= 1.0) {

               segments.add(1, new EvaluationSegment(Qe, new Point3D(xJ, yJ, zJ),
                  QScr, JScr, iQ, -1, currentTria + 1));
            }


         }

         segments.remove(0);
         currentTest = 1;
         currentTria = 0;

         if (IScr == null) {
         	if (JScr == null) {
         		hidden.add(new CompletedSegment(PScr, QScr));
         	} else {
            	    hidden.add(new CompletedSegment(PScr, JScr));
         	}
         } else {
         	if (JScr == null) {
         		hidden.add(new CompletedSegment(IScr, QScr));

         	} else {
         		hidden.add(new CompletedSegment(IScr, JScr));
         	}
         }
         return;
            // if no continue-statement has been executed
         }

   }

   //"Old" lineSegment method used for standard non-test drawing

private void oldLineSegment(Graphics g, Point3D Pe, Point3D Qe,
	      Point2D PScr, Point2D QScr, int iP, int iQ, int iStart)
	   {  Point2D IScr = null;
	   	  Point2D JScr = null;
	   	  double u1 = QScr.x - PScr.x, u2 = QScr.y - PScr.y;
	      double minPQx = Math.min(PScr.x, QScr.x);
	      double maxPQx = Math.max(PScr.x, QScr.x);
	      double minPQy = Math.min(PScr.y, QScr.y);
	      double maxPQy = Math.max(PScr.y, QScr.y);
	      double zP = Pe.z, zQ = Qe.z;
	      double minPQz = Math.min(zP, zQ);
	      Point3D[] e = obj.getE();
	      Point2D[] vScr = obj.getVScr();

	      for (int i=iStart; i<nTria; i++)
	      {  Tria t = tr[i];
	         int iA = t.iA, iB = t.iB, iC = t.iC;
	         Point2D AScr = vScr[iA], BScr = vScr[iB], CScr = vScr[iC];
	         @SuppressWarnings("unused")
			boolean segment = false;
	         // 1. Minimax test for x and y screen coordinates:
	         if (maxPQx <= AScr.x && maxPQx <= BScr.x && maxPQx <= CScr.x
	          || minPQx >= AScr.x && minPQx >= BScr.x && minPQx >= CScr.x
	          || maxPQy <= AScr.y && maxPQy <= BScr.y && maxPQy <= CScr.y
	          || minPQy >= AScr.y && minPQy >= BScr.y && minPQy >= CScr.y)
	             continue;

	         // 2. Test if PQ is an edge of ABC:
	         if ((iP == iA || iP == iB || iP == iC) &&
	             (iQ == iA || iQ == iB || iQ == iC)) continue;

	         // 3. Test if PQ is clearly nearer than ABC:
	         Point3D Ae = e[iA], Be = e[iB], Ce = e[iC];
	         double zA = Ae.z, zB = Be.z, zC = Ce.z;
	         if (minPQz >= zA && minPQz >= zB && minPQz >= zC) continue;

	         // 4. Do P and Q (in 2D) lie in a half plane defined
	         //    by line AB, on the side other than that of C?
	         //    Similar for the edges BC and CA.
	         double eps = 0.1; // Relative to numbers of pixels
	         if (Tools2D.area2(AScr, BScr, PScr) < eps &&
	             Tools2D.area2(AScr, BScr, QScr) < eps ||
	             Tools2D.area2(BScr, CScr, PScr) < eps &&
	             Tools2D.area2(BScr, CScr, QScr) < eps ||
	             Tools2D.area2(CScr, AScr, PScr) < eps &&
	             Tools2D.area2(CScr, AScr, QScr) < eps) continue;

	         // 5. Test (2D) if A, B and C lie on the same side
	         //    of the infinite line through P and Q:
	         double PQA = Tools2D.area2(PScr, QScr, AScr);
	         double PQB = Tools2D.area2(PScr, QScr, BScr);
	         double PQC = Tools2D.area2(PScr, QScr, CScr);

	         if (PQA < +eps && PQB < +eps && PQC < +eps ||
	             PQA > -eps && PQB > -eps && PQC > -eps)
	            continue;

	         // 6. Test if neither P nor Q lies behind the
	         //    infinite plane through A, B and C:
	         int iPol = refPol[i];
	         Polygon3D pol = (Polygon3D)polyList.elementAt(iPol);
	         double a = pol.getA(), b = pol.getB(), c = pol.getC(),
	            h = pol.getH(), eps1 = 1e-5 * Math.abs(h),
	            hP = a * Pe.x + b * Pe.y + c * Pe.z,
	            hQ = a * Qe.x + b * Qe.y + c * Qe.z;
	         if (hP > h - eps1 && hQ > h - eps1)
	            continue;

	         // 7. Test if both P and Q behind triangle ABC:
	         boolean PInside =
	            Tools2D.insideTriangle(AScr, BScr, CScr, PScr);
	         boolean QInside =
	            Tools2D.insideTriangle(AScr, BScr, CScr, QScr);
	         if (PInside && QInside) {
	         	return;
	         }


	         // 8. If P nearer than ABC and inside, PQ visible;
	         //    the same for Q:
	         double h1 = h + eps1;
	         boolean PNear = hP > h1, QNear = hQ > h1;
	         if (PNear && PInside || QNear && QInside) continue;

	         // 9. Compute the intersections I and J of PQ
	         // with ABC in 2D.
	         // If, in 3D, such an intersection lies in front of
	         // ABC, this triangle does not obscure PQ.
	         // Otherwise, the intersections lie behind ABC and
	         // this triangle obscures part of PQ:
	         double lambdaMin = 1.0, lambdaMax = 0.0;
	         for (int ii=0; ii<3; ii++)
	         {  double v1 = BScr.x - AScr.x, v2 = BScr.y - AScr.y,
	                   w1 = AScr.x - PScr.x, w2 = AScr.y - PScr.y,
	                   denom = u2 * v1 - u1 * v2;
	            if (denom != 0)
	            {  double mu = (u1 * w2 - u2 * w1)/denom;
	               // mu = 0 gives A and mu = 1 gives B.
	               if (mu > -0.0001 && mu < 1.0001)
	               {  double lambda = (v1 * w2 - v2 * w1)/denom;
	                  // lambda = PI/PQ
	                  // (I is point of intersection)
	                  if (lambda > -0.0001 && lambda < 1.0001)
	                  {  if (PInside != QInside &&
	                     lambda > 0.0001 && lambda < 0.9999)
	                     {  lambdaMin = lambdaMax = lambda;
	                        break;
	                        // Only one point of intersection
	                     }
	                     if (lambda < lambdaMin) lambdaMin = lambda;
	                     if (lambda > lambdaMax) lambdaMax = lambda;
	                  }
	               }
	            }
	            Point2D temp = AScr; AScr = BScr;
	            BScr = CScr; CScr = temp;
	         }
	         float d = obj.getD();
	         if (!PInside && lambdaMin > 0.001)
	         {  double IScrx = PScr.x + lambdaMin * u1,
	                   IScry = PScr.y + lambdaMin * u2;
	            // Back from screen to eye coordinates:
	            double zI = 1/(lambdaMin/zQ + (1 - lambdaMin)/zP),
	                   xI = -zI * IScrx / d, yI = -zI * IScry / d;
	            if (a * xI + b * yI + c * zI > h1) continue;
	            IScr = new Point2D((float)IScrx, (float)IScry);
	            if (Tools2D.distance2(IScr, PScr) >= 1.0) {

	               oldLineSegment(g, Pe, new Point3D(xI, yI, zI), PScr,
	                  IScr, iP, -1, i + 1);
	            }
	         }
	         if (!QInside && lambdaMax < 0.999)
	         {  double JScrx = PScr.x + lambdaMax * u1,
	                   JScry = PScr.y + lambdaMax * u2;
	            double zJ =
	               1/(lambdaMax/zQ + (1 - lambdaMax)/zP),
	                  xJ = -zJ * JScrx / d, yJ = -zJ * JScry / d;
	            if (a * xJ + b * yJ + c * zJ > h1) continue;
	            JScr = new Point2D((float)JScrx, (float)JScry);
	            if (Tools2D.distance2(JScr, QScr) >= 1.0) {


	               oldLineSegment(g, Qe, new Point3D(xJ, yJ, zJ),
	                  QScr, JScr, iQ, -1, i + 1);
	            }
	         }
	         return;

	            // if no continue-statement has been executed
	      }
	      g.setColor(Color.BLACK);
	      drawLine(g, PScr.x, PScr.y, QScr.x, QScr.y);
	   }
}

//simple animation thread linked to the canvas
class AnimationThread extends Thread{
	int currentFrame, limit;
	CvHLines host;
	public AnimationThread(CvHLines h) {
		host = h;
		limit = 0;
		currentFrame = 0;
	}

	public void init(int frameLimit) {
		limit = frameLimit;
		currentFrame = 0;
	}

	public void reset() {
		currentFrame = 0;
	}
	public void run() {
		while (true) {
			while (host.animation_mode && currentFrame < limit) {
				currentFrame++;
				host.repaint();
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {

				}
			}
		}
	}
}
//Data structure which contains all the necessary information
//for a call to lineSegment.
class EvaluationSegment {
	Point3D Pe;
	Point3D Qe;
    Point2D PScr;
    Point2D QScr;
    int iP; int iQ; int iStart;

	EvaluationSegment(Point3D p1, Point3D p2, Point2D p3, Point2D p4,
			         int p, int q, int i) {
		Pe = p1;
		Qe = p2;
		PScr = p3;
		QScr = p4;
		iP = p;
		iQ = q;
		iStart = i;

	}
}

//Data structure representing the screen coordinates of line segment
//that has passed evaluation and is either visible or hidden.
class CompletedSegment {
	Point2D PScr, QScr;

	CompletedSegment(Point2D p, Point2D q) {
		PScr = p;
		QScr = q;
	}
}

class myHPGL
{  FileWriter fw;
   myHPGL(Obj3D obj)
   {  String plotFileName = "", fName = obj.getFName();
      for (int i=0; i<fName.length(); i++)
      {  char ch = fName.charAt(i);
         if (ch == '.') break;
         plotFileName += ch;
      }
      plotFileName += ".plt";
      try
      {  fw = new FileWriter(plotFileName);
         fw.write("IN;SP1;\n");
      }
      catch (IOException ioe){}
   }

   void write(String s)
   {  try {fw.write(s); fw.flush();}catch (IOException ioe){}
   }
}
