package trabalho;// DemoDisplayFrame.java:
//	Sets up the display frame for the demo.

/* CGDemo is a companion of the textbook

L. Ammeraal and K. Zhang, Computer Graphics for Java Programmers, 
2nd Edition, Wiley, 2006.

Copyright (C) 2006  Janis Schubert, Kang Zhang, Leen Ammeraal 

This program is free software; you can redistribute it and/or 
modify it under the terms of the GNU General Public License as 
published by the Free Software Foundation; either version 2 of 
the License, or (at your option) any later version. 

This program is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of 
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
See the GNU General Public License for more details.  

You should have received a copy of the GNU General Public 
License along with this program; if not, write to 
the Free Software Foundation, Inc., 51 Franklin Street, 
Fifth Floor, Boston, MA  02110-1301, USA. 
*/

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class DemoDisplayFrame extends JFrame
{
	/**	 * 	 */	private static final long	serialVersionUID	= 1L;	SplashWindow splash = new SplashWindow("CGDemoSplash.jpg", this, 7000);
	final static String PONTO = "Ponto";
	final static String LINHA = "Linha";
	final static String TRIANGULO = "Triangulo";
	final static String RETANGULO = "Retangulo";
      final static String CIRCULO = "Circulo";

	DemoDisplayFrame()
	{
		setTitle("Trabalho Bimestral PDI [CC] [UDC] [2017]");

		addWindowListener(new WindowAdapter()
			{
				public void windowClosing(WindowEvent e)
				{
					System.exit(0);
				}
			} );

		Container contentPane = getContentPane();
		JTabbedPane tabbedPane = new JTabbedPane();
		Ponto ponto = new Ponto();
		Linha linha = new Linha();
		Triangulo triangulo = new Triangulo();
		Retangulo retangulo = new Retangulo();
		Circulo circulo = new Circulo();

		tabbedPane.addTab(PONTO, ponto);
		tabbedPane.addTab(LINHA, linha);
		tabbedPane.addTab(TRIANGULO, triangulo);
		tabbedPane.addTab(RETANGULO, retangulo);
		tabbedPane.addTab(CIRCULO, circulo);

		contentPane.add(tabbedPane, BorderLayout.CENTER);
	}
}
