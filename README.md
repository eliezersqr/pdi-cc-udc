Centro Universitário União Dinâmica das Cataratas - Foz do Iguaçu

Aluno: Eliézer de Siqueira

Professor: Miguel Matrakas

Disciplina: Processamento Digital de Imagens

Curso: Ciência da Computação

Trabalho 2º Bimestre - 2017.

A implementação pode ser realizada utilizando as linguagens Java, C/C++ ou C#;
Não podem ser utilizadas funcionalidades da API da linguagem para o desenho de primitivas gráficas além do ponto (serão permitidas chamadas aos serviços de troca de cor e espessura de linha);
Podem ser utilizados como base para o desenvolvimento do trabalho os códigos disponíveis no livro: "Computação Gráfica para programadores Java" (adaptados à linguagem do trabalho);
As funcionalidades a serem implementadas no trabalho serão informadas em sala de aula, e em seguida atualizadas nesta lista;
Definir as características do universo: dimensões (2D ou 3D), unidade de trabalho, janela de visualização, etc;
Indicar se a janela de visualização deve utilizar modo isotrópico ou anisotrópico Apenas isotrópico;
Disponibilizar métodos de entrada de dados para definir elementos (também chamados de modelos), que correspondem a objetos presentes no universo;
Ponto;
Linha;
Triângulo;
Retângulo;
Círculo;
Curvas - pelo menos uma das seguintes:
Hermite;
Bezier;
Spline;
Implementar no mínimo as seguintes transformações geométricas a serem aplicadas no modelos:
Translação;
Escala;
Rotação;
Reflexão;
Cisalhamento;
Uma transformação extra de sua escolha;
Apresentar os modelos definidos no universo de maneira correta na janela de visualização configurada;
Desejável uma forma de persistência dos dados dos modelos definidos.